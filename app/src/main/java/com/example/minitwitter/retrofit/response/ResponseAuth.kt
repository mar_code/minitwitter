package com.example.minitwitter.retrofit.response

import com.google.gson.annotations.SerializedName

class ResponseAuth {
    @SerializedName("token")
    var token: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("photoUrl")
    var photoUrl: String? = null

    @SerializedName("created")
    var created: String? = null

    @SerializedName("active")
    var active: Boolean? = null

    constructor() {}
    constructor(token: String?, username: String?, email: String?, photoUrl: String?, created: String?, active: Boolean?) : super() {
        this.token = token
        this.username = username
        this.email = email
        this.photoUrl = photoUrl
        this.created = created
        this.active = active
    }
}