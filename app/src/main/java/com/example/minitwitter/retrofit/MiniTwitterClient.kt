package com.example.minitwitter.retrofit

import com.example.minitwitter.common.Constantes
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MiniTwitterClient {

    constructor() {
        retrofit = Retrofit.Builder()
            .baseUrl(Constantes.API_MINITWITTER_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        miniTwitterService = retrofit!!.create(MiniTwitterService::class.java)
    }

    companion object {

        private var miniTwitterService: MiniTwitterService? = null
        private var retrofit: Retrofit? = null
        private var instance: MiniTwitterClient? = null

        //Patròn Singleton
        fun getInstancea():MiniTwitterClient {
            if(instance == null)
            {
                instance = MiniTwitterClient()
            }
            return instance as MiniTwitterClient
        }

        fun getMiniTwitterService(): MiniTwitterService? {
            return miniTwitterService
        }

    }

}
