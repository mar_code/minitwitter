package com.example.minitwitter.retrofit

import com.example.minitwitter.common.Constantes
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AuthMiniTwitterClient {

    constructor() {
        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.addInterceptor(AuthInterceptor())
        val cliente = okHttpClientBuilder.build()

        retrofit = Retrofit.Builder()
            .baseUrl(Constantes.API_MINITWITTER_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(cliente)
            .build()
        authMiniTwitterService = retrofit!!.create(AuthMiniTwitterService::class.java)
    }

    companion object {

        private var authMiniTwitterService: AuthMiniTwitterService? = null
        private var retrofit: Retrofit? = null
        private var instance: AuthMiniTwitterClient? = null

        //Patròn Singleton
        fun getInstance():AuthMiniTwitterClient {
            if(instance == null)
            {
                instance = AuthMiniTwitterClient()
            }
            return instance as AuthMiniTwitterClient
        }

        fun getAuthMiniTwitterService(): AuthMiniTwitterService? {
            return authMiniTwitterService
        }

    }

}
