package com.example.minitwitter.retrofit.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Tweet {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("mensaje")
    var mensaje: String? = null

    @SerializedName("likes")
    var likes: List<Like> = ArrayList<Like>()

    @SerializedName("user")
    var user: User? = null

    constructor() {}

    constructor(id: Int?, mensaje: String?, likes: List<Like>, user: User?) : super() {
        this.id = id
        this.mensaje = mensaje
        this.likes = likes
        this.user = user
    }
}