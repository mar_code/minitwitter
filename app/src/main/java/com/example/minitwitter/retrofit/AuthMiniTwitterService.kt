package com.example.minitwitter.retrofit

import com.example.minitwitter.retrofit.response.Tweet
import retrofit2.Call
import retrofit2.http.GET

interface AuthMiniTwitterService {
    @GET("tweets/all")
    fun getAllTweets(): Call<List<Tweet>>?
}