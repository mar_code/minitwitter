package com.example.minitwitter.retrofit.request

import com.google.gson.annotations.SerializedName

class RequestLogin {
    @SerializedName("email")
    private var email: String? = null

    @SerializedName("password")
    private var password: String? = null

    constructor() {}
    constructor(email: String?, password: String?) : super() {
        this.email = email
        this.password = password
    }
}