package com.example.minitwitter.retrofit.request

import com.google.gson.annotations.SerializedName

class RequestSignup {
    @SerializedName("username")
    private var username: String? = null

    @SerializedName("email")
    private var email: String? = null

    @SerializedName("password")
    private var password: String? = null

    @SerializedName("code")
    private var code: String? = null

    constructor() {}
    constructor(
        username: String?,
        email: String?,
        password: String?,
        code: String?
    ) : super() {
        this.username = username
        this.email = email
        this.password = password
        this.code = code
    }
}