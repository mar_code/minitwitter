package com.example.minitwitter.retrofit.response

import com.google.gson.annotations.SerializedName


class User {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("descripcion")
    var descripcion: String? = null

    @SerializedName("website")
    var website: String? = null

    @SerializedName("photoUrl")
    var photoUrl: String? = null

    @SerializedName("created")
    var created: String? = null

    constructor() {}

    constructor(id: Int?, username: String?, descripcion: String?, website: String?, photoUrl: String?, created: String?) : super() {
        this.id = id
        this.username = username
        this.descripcion = descripcion
        this.website = website
        this.photoUrl = photoUrl
        this.created = created
    }
}