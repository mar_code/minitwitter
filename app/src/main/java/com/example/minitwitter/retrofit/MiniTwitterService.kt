package com.example.minitwitter.retrofit

import com.example.minitwitter.retrofit.request.RequestLogin
import com.example.minitwitter.retrofit.request.RequestSignup
import com.example.minitwitter.retrofit.response.ResponseAuth
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface MiniTwitterService {
    @POST("auth/login")
    fun doLogin(@Body requestLogin: RequestLogin?): Call<ResponseAuth?>?

    @POST("auth/signup")
    fun doSignup(@Body requestSignup: RequestSignup?): Call<ResponseAuth?>?
}
