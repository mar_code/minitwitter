package com.example.minitwitter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.minitwitter.data.TweetViewModel
import com.example.minitwitter.retrofit.AuthMiniTwitterClient
import com.example.minitwitter.retrofit.AuthMiniTwitterService
import com.example.minitwitter.retrofit.MiniTwitterClient
import com.example.minitwitter.retrofit.MiniTwitterService
import com.example.minitwitter.retrofit.response.ResponseAuth
import com.example.minitwitter.retrofit.response.Tweet
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TweetListFragment : Fragment() {

    private var columnCount = 1
    private var tweetList : List<Tweet>? = null
    private var tweetViewModel : TweetViewModel? = null
    private var adapterTweet : MyTweetRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        tweetViewModel = ViewModelProvider(requireActivity()).get(TweetViewModel::class.java)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_tweet_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }

                adapterTweet = MyTweetRecyclerViewAdapter(requireActivity().baseContext, tweetList)
                adapter = adapterTweet

                tweetViewModel!!.getTweets().observe(requireActivity(),
                    Observer<List<Tweet>> { tweets ->
                        tweetList = tweets
                        adapterTweet?.setData(tweetList!!)
                    })
            }
        }
        return view
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            TweetListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}