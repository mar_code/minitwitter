package com.example.minitwitter.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.minitwitter.retrofit.response.Tweet

class TweetViewModel(aplication: Application) : AndroidViewModel(aplication) {
    private var tweetRepository : TweetRepository? = null
    private var tweets : LiveData<List<Tweet>>? = null

    init {
        tweetRepository = TweetRepository()
        tweets = tweetRepository!!.getAllTweets()
    }

    fun getTweets() : LiveData<List<Tweet>>{
        return tweets!!
    }
}