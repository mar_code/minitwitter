package com.example.minitwitter.data

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.minitwitter.common.MyApp
import com.example.minitwitter.retrofit.AuthMiniTwitterClient
import com.example.minitwitter.retrofit.AuthMiniTwitterService
import com.example.minitwitter.retrofit.response.Tweet
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TweetRepository {
    private var authMiniTwitterClient: AuthMiniTwitterClient? = null
    private var authMiniTwitterService: AuthMiniTwitterService? = null
    private val data = MutableLiveData<List<Tweet>>()
    private var allTweets: LiveData<List<Tweet>> = data

    init {
        authMiniTwitterClient = AuthMiniTwitterClient.getInstance()
        authMiniTwitterService = AuthMiniTwitterClient.getAuthMiniTwitterService()
        allTweets = getAllTweets()!!
    }

    fun getAllTweets(): LiveData<List<Tweet>>? {

        val call: Call<List<Tweet>>? = authMiniTwitterService?.getAllTweets()
        call?.enqueue(object : Callback<List<Tweet>> {
            override fun onResponse(call: Call<List<Tweet>>, response: Response<List<Tweet>>) {
                if(response.isSuccessful){
                    data.value = response.body()
                } else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<List<Tweet>>, t: Throwable) {
                Toast.makeText(MyApp.getContext(), "Verifique su conexión a internet", Toast.LENGTH_SHORT).show()
            }

        })

        return data
    }
}