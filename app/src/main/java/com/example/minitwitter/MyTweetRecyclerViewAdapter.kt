package com.example.minitwitter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import com.bumptech.glide.Glide
import com.example.minitwitter.common.Constantes
import com.example.minitwitter.common.SharedPreferencesManager
import com.example.minitwitter.retrofit.response.Tweet
import kotlinx.android.synthetic.main.fragment_tweet.view.*

class MyTweetRecyclerViewAdapter(private val ctx: Context, private var values: List<Tweet>?) : RecyclerView.Adapter<MyTweetRecyclerViewAdapter.ViewHolder>() {

    private var userName = SharedPreferencesManager.getSomeStringValue(Constantes.PREF_USERNAME)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_tweet, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(values != null){
            val item = values!![position]
            var photo  = item.user?.photoUrl

            holder.tvUsername.text = item.user?.username
            holder.tvMessage.text = item.mensaje
            holder.tvLikesCount.text = item.likes.size.toString()
            if(!photo.equals("")) {
                Glide.with(ctx)
                    .load("https://www.minitwitter.com/apiv1/uploads/photos/$photo")
                    .into(holder.ivAvatar)
            }

            for(like in item.likes) {
                if(like.username?.equals(userName)!!){
                    Glide.with(ctx)
                        .load(R.drawable.ic_like_full_pink)
                        .into(holder.ivLike)
                    holder.tvLikesCount.setTextColor(ctx.resources.getColor(R.color.colorPink))
                    holder.tvLikesCount.setTypeface(null, Typeface.BOLD)
                }
            }
        }
    }

    fun setData(tweetList: List<Tweet>){
        this.values = tweetList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int{
        if(values != null)
            return values!!.size
        else
            return 0
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var ivAvatar: ImageView = view.ImageViewAvatar
        var ivLike: ImageView = view.ImageViewLike
        var tvUsername: TextView = view.TextViewUsername
        var tvMessage: TextView = view.TextViewMessage
        var tvLikesCount: TextView = view.TextViewLikes
    }
}