package com.example.minitwitter.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.minitwitter.R
import com.example.minitwitter.common.Constantes
import com.example.minitwitter.common.SharedPreferencesManager
import com.example.minitwitter.retrofit.MiniTwitterClient
import com.example.minitwitter.retrofit.MiniTwitterService
import com.example.minitwitter.retrofit.request.RequestLogin
import com.example.minitwitter.retrofit.response.ResponseAuth
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private var miniTwitterClient:MiniTwitterClient? = null
    private var miniTwitterService:MiniTwitterService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //inicializar Retrofrit
        retrofitInit()

        //Loguearse
        btnLogin.setOnClickListener {
            goToLogin()
        }

        //Ir a Registrarse
        tvGoSignUp.setOnClickListener {
            goToSignUp()
        }
    }

    private fun goToLogin() {
        if(etEmail.text.toString().isEmpty()){
            etEmail.error = "Se requiere el Email"
        }
        else if(etPassword.text.toString().isEmpty()){
            etPassword.error = "Se requiere la contraseña"
        }
        else{
            var requestLogin = RequestLogin(etEmail.getText().toString(), etPassword.getText().toString())
            var call: Call<ResponseAuth?>? = miniTwitterService?.doLogin(requestLogin)

            call?.enqueue(object : Callback<ResponseAuth?>{
                override fun onFailure(call: Call<ResponseAuth?>, t: Throwable) {
                    Toast.makeText(this@MainActivity, "Error al consumir el servicio", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<ResponseAuth?>, response: Response<ResponseAuth?>) {
                    var responseAuth:ResponseAuth? = response.body()

                    if (response.isSuccessful)
                    {
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_TOKEN, response.body()?.token!!)
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_USERNAME, response.body()?.username!!)
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_EMAIL, response.body()?.email!!)
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_PHOTOURL, response.body()?.photoUrl!!)
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_CREATED, response.body()?.created!!)
                        SharedPreferencesManager.setSomeBooleanValue(Constantes.PREF_ACTIVE, response.body()?.active!!)

                        val intent = Intent(this@MainActivity, DashboardActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else
                    {
                        Toast.makeText(this@MainActivity, "Datos incorrectos", Toast.LENGTH_SHORT).show()
                    }
                }

            })
        }
    }

    private fun goToSignUp()
    {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }

    private fun retrofitInit(){
        miniTwitterClient = MiniTwitterClient.getInstancea()
        miniTwitterService = MiniTwitterClient.getMiniTwitterService()
    }
}