package com.example.minitwitter.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.minitwitter.R
import com.example.minitwitter.common.Constantes
import com.example.minitwitter.common.SharedPreferencesManager
import com.example.minitwitter.retrofit.MiniTwitterClient
import com.example.minitwitter.retrofit.MiniTwitterService
import com.example.minitwitter.retrofit.request.RequestSignup
import com.example.minitwitter.retrofit.response.ResponseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUpActivity : AppCompatActivity() {

    private var miniTwitterClient: MiniTwitterClient? = null
    private var miniTwitterService: MiniTwitterService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        retrofitInit()

        btnSignUp.setOnClickListener {
            goToSignUp()
        }
    }

    override fun onBackPressed() {
        finish()
    }

    private fun goToSignUp()
    {
        if(etUsernameSign.text.toString().isEmpty()){
            etUsernameSign.error = "El nombre de usuario es requerido"
        } else if(etEmailSign.text.toString().isEmpty()){
            etEmailSign.error = "El email es requerido"
        } else if(etPasswordSign.text.toString().isEmpty() || etPasswordSign.text.toString().length < 4){
            etPasswordSign.error = "La contraseña es requerida y debe tener al menos 4 caracteres"
        } else{
            val requestSignup = RequestSignup(etUsernameSign.text.toString(), etEmailSign.text.toString(), etPasswordSign.text.toString(), "UDEMYANDROID")
            var call: Call<ResponseAuth?>? = miniTwitterService?.doSignup(requestSignup)

            call?.enqueue(object : Callback<ResponseAuth?>{
                override fun onResponse(call: Call<ResponseAuth?>, response: Response<ResponseAuth?>) {
                    if(response.isSuccessful){

                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_TOKEN, response.body()?.token!!)
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_USERNAME, response.body()?.username!!)
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_EMAIL, response.body()?.email!!)
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_PHOTOURL, response.body()?.photoUrl!!)
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_CREATED, response.body()?.created!!)
                        SharedPreferencesManager.setSomeBooleanValue(Constantes.PREF_ACTIVE, response.body()?.active!!)

                        val intent = Intent(this@SignUpActivity, DashboardActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else
                    {
                        Toast.makeText(this@SignUpActivity, "Error, revise los datos", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ResponseAuth?>, t: Throwable) {
                    Toast.makeText(this@SignUpActivity, "Error en el servicio", Toast.LENGTH_SHORT).show()
                }

            })
        }
    }

    private fun retrofitInit(){
        miniTwitterClient = MiniTwitterClient.getInstancea()
        miniTwitterService = MiniTwitterClient.getMiniTwitterService()
    }
}