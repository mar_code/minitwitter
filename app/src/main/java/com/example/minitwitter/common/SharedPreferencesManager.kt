package com.example.minitwitter.common

import android.R
import android.content.Context
import android.content.SharedPreferences


class SharedPreferencesManager {

    companion object{
        private const val APP_SETTINGS_FILE:String = "APP_SETTINGS"

        private fun getSharedPreferences():SharedPreferences{
            return MyApp.getContext()?.getSharedPreferences(APP_SETTINGS_FILE, Context.MODE_PRIVATE)!!
        }

        fun setSomeStringValue(dataKey: String, dataValue: String){
            val editor: SharedPreferences.Editor = getSharedPreferences().edit()
            editor.putString(dataKey, dataValue)
            editor.apply()
        }

        fun setSomeBooleanValue(dataKey: String, dataValue: Boolean){
            val editor: SharedPreferences.Editor = getSharedPreferences().edit()
            editor.putBoolean(dataKey, dataValue)
            editor.apply()
        }

        fun getSomeStringValue(dataKey: String): String {
            return getSharedPreferences().getString(dataKey, "")!!
        }

        fun getSomeBooleanValue(dataKey: String): Boolean {
            return getSharedPreferences().getBoolean(dataKey, false)
        }
    }
}