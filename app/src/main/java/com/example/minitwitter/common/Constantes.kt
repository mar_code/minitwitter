package com.example.minitwitter.common

class Constantes {

    companion object{
        const val API_MINITWITTER_BASE_URL = "https://www.minitwitter.com:3001/apiv1/"

        //PREFERENCES
        const val PREF_TOKEN = "PREF_TOKEN"
        const val PREF_USERNAME = "PREF_USERNAME"
        const val PREF_EMAIL = "PREF_EMAIL"
        const val PREF_PHOTOURL = "PREF_PHOTOURL"
        const val PREF_CREATED = "PREF_CREATED"
        const val PREF_ACTIVE = "PREF_ACTIVE"
    }
}